/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONTESTER_MUONSMATRIXBRANCH_IXX
#define MUONTESTER_MUONSMATRIXBRANCH_IXX

namespace MuonVal {


template <class T> MatrixBranch<T>::MatrixBranch(TTree* tree, const std::string& name) : 
    VectorBranch<std::vector<T>>(tree, name) {}
template <class T> MatrixBranch<T>::MatrixBranch(TTree* tree, const std::string& b_name, const T& def) : 
    MatrixBranch<T>(tree, b_name) {
    setDefault(def);
}
template <class T> MatrixBranch<T>::MatrixBranch(MuonTesterTree& parent, const std::string& name) : 
    VectorBranch<std::vector<T>>{parent, name} {}
template <class T>
MatrixBranch<T>::MatrixBranch(MuonTesterTree& tree, const std::string& b_name, const T& def) : 
    MatrixBranch<T>(tree, b_name) {
    setDefault(def);
}
template <class T> void MatrixBranch<T>::push_back(size_t i, const T& value) { 
    get(i).push_back(value); }
template <class T> T& MatrixBranch<T>::get(size_t i, size_t j) {
    
    if (get(i).size() <= j) {
        for (size_t k = get(i).size(); k <= j; ++k) { get(i).push_back(getDefault()); }
    }
    return get(i)[j];
}
template <class T> size_t MatrixBranch<T>::nrows() const { return VectorBranch<std::vector<T>>::size(); }
template <class T> size_t MatrixBranch<T>::ncols(size_t row) const {
    if (row >= nrows()) { return 0; }
    return get(row).size();
}
template <class T> const T& MatrixBranch<T>::getDefault() const { return m_default; }
template <class T> void MatrixBranch<T>::setDefault(const T& def) { m_default = def; }

}
#endif